<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {

        $all_makanan = [
             [
                'nama' => 'Chicken Chop',
                'gambar' => 'https://images.unsplash.com/photo-1432139555190-58524dae6a55?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8Y2hpY2tlbiUyMGNob3B8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, quasi. Molestias, debitis nemo? Vel explicabo at amet vero, fugit odit porro aut voluptate. Facere assumenda tempora accusamus cum, exercitationem esse?'
             ], [
            'nama' => 'Lamb Chop',
            'gambar' => 'https://images.unsplash.com/photo-1550367363-ea12860cc124?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bGFtYiUyMGNob3B8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, quasi. Molestias, debitis nemo? Vel explicabo at amet vero, fugit odit porro aut voluptate. Facere assumenda tempora accusamus cum, exercitationem esse?'
            ], [
            'nama' => 'Fish Chip',
            'gambar' => 'https://images.unsplash.com/photo-1579208030886-b937da0925dc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8ZmlzaCUyMCUyNiUyMGNoaXBzfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, quasi. Molestias, debitis nemo? Vel explicabo at amet vero, fugit odit porro aut voluptate. Facere assumenda tempora accusamus cum, exercitationem esse?'
            ], [
            'nama' => 'Briyani',
            'gambar' => 'https://images.unsplash.com/photo-1642821373475-cfd6c7301b18?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YnJpeWFuaXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, quasi. Molestias, debitis nemo? Vel explicabo at amet vero, fugit odit porro aut voluptate. Facere assumenda tempora accusamus cum, exercitationem esse?'
            ], [
            'nama' => 'Sushi',
            'gambar' => 'https://images.unsplash.com/photo-1553621042-f6e147245754?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8c3VzaGl8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, quasi. Molestias, debitis nemo? Vel explicabo at amet vero, fugit odit porro aut voluptate. Facere assumenda tempora accusamus cum, exercitationem esse?'
            ]
            
        ];

        return view('homepage', [ 'all_makanan' => $all_makanan ]);
    }
}
